package jp.alhinc.ogawa_ayaka.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		if(args.length != 1) { //コマンドライン引数の例外処理
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String,String> branchlist = new HashMap<String,String>();//支店コードと支店名が入ったマップ
		Map<String,Long>SalseValues = new HashMap<String,Long>(); //支店コードと売り上げ値が入るマップ

		// TODO 自動生成されたメソッド・スタブ

		if(!readFile(args[0] , "branch.lst" , branchlist , SalseValues)) {
			return;
		}


		File dir = new File(args[0]) ; //args[0]のコマンドライン引数の中からdirを指定
		File[] Files = dir.listFiles();//指定したdirから複数あるファイルの配列を作る（list）

		List<File>salesFiles = new ArrayList<File>();


		for(int i = 0 ; i < Files.length ; i++) {       //listの中の複数あるファイルの中から該当するものを検索するのを繰り返す
			if(Files[i].getName().matches("^[0-9]{8}"+".rcd$") && Files[i].isFile()) { //売り上げファイルを検索,ファイルかどうかの判別
				salesFiles.add(Files[i]);//売り上げファイルを保持
			}
		}
		for(int i = 0; i < salesFiles.size() -1 ; i++) {
			String salesFilesnum1 = salesFiles.get(i).getName();
			String salesFiles1 = salesFilesnum1.substring(0,8);

			String salesFilessnum2 = salesFiles.get(i + 1).getName();
			String salesFiles2 = salesFilessnum2.substring(0,8);

			int salesFileNum1 = Integer.parseInt(salesFiles1);
			int salesFileNum2 = Integer.parseInt(salesFiles2);

			if(salesFileNum1 != salesFileNum2 -1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		BufferedReader br1 = null;

		for(int i  =0 ; i < salesFiles.size() ; i++) { //売りあげファイルの数だけ繰り返す
			try {
				FileReader fr1 = new FileReader(salesFiles.get(i));
				br1 = new BufferedReader(fr1);

				List<String> valuelist = new ArrayList<String>() ; //売り上げファイルを読み込み、その値をlist1で保持

				String line;

				while((line = br1.readLine()) != null) {

					valuelist.add(line);
				}
				String salesFileline = 	salesFiles.get(i).getName();
				if(valuelist.size() != 2) {
					System.out.println(salesFileline + "のフォーマットが不正です");
					return;
				}

				String salesFilecoad = 	salesFiles.get(i).getName();

				if(!branchlist.containsKey(valuelist.get(0))) {
						System.out.println(salesFilecoad + "の支店コードが不正です");
						return;
				}

				if(!valuelist.get(1).matches( "[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}



				long value1 = Long.parseLong(valuelist.get(1)); //Ssalsevalueのマップの売り上げ(0円)値はlong型だからstring型に変換必要



                value1 += SalseValues.get(valuelist.get(0));//Mapの中の0円と売り上げ金額を加算

                if(value1 >= 10000000000L) {
                	System.out.println("合計金額が10桁を超えました");
                	return;
                }

			    SalseValues.put(valuelist.get(0) ,value1);//売り上げをMapに入れた


			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br1 !=null) {
					try {
						br1.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}







		if(!writeFile(args[0] , branchlist , SalseValues , "branch.out")) {
			return;
		}




	}
	public static boolean writeFile(String dir ,Map<String, String> branchlist1 , Map<String, Long> SalseValues, String fileName){

		BufferedWriter bw = null;
		 try {
			 //blanch.outのファイルへ出力
			 	File branchoutfiles = new File(dir,fileName);
			 	FileWriter fw = new FileWriter(branchoutfiles);
			 	bw = new BufferedWriter(fw);
			 	//keyを一括で取得
			 	for(String branchoutkey:  branchlist1.keySet()){

			 		bw.write(branchoutkey + "," + branchlist1.get(branchoutkey) + "," +SalseValues.get(branchoutkey));
			 		bw.newLine(); //改行処理
			 	}

		 }catch(IOException e){
			 System.out.println("予期せぬエラーが発生しました");
			 return false;

		}finally {
	    	 if(bw != null) {
	    		 try {
	    			 bw.close();
	    		 }catch(IOException e) {
	    			 System.out.println("予期せぬエラーが発生しました");
	    			 return false;
	    		 }
	    	 }
	     }
		 return true;
	}


	public static boolean readFile(String dir , String fileName , Map<String, String> branchlist1 , Map<String, Long> SalseValues) {
		BufferedReader br = null;
		try {
			File file = new File(dir,fileName);
			if(! file.exists() || ! file.isFile()) { //ファイルの存在確認かつそれがファイルかどうか判定
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);

			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

                String[] coad = line.split(",",0); // lineで分けた支店コードと支店名は変数coad
                if(coad.length != 2) {
                	System.out.println("支店定義ファイルのフォーマットが不正です");
                	return false;
                }
                String branchcoad = coad[0];
                if( ! branchcoad.matches("^[0-9]{3}")) {
                	System.out.println("支店定義ファイルのフォーマットが不正です");
                	return false;
                }
              branchlist1.put( coad[0] , coad[1]);
              SalseValues.put( coad[0], 0L); //売り上げファイルの中を（支店コード、売り上げは０円のまま、Lはlong型の際に使用）
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}return true;
	}

}



